module.exports = function(app){
	var controller = {};
	
	var User = app.models.user;
	
	controller.save = function(req, res){
		var _id = req.body._id;
		
		if(_id){
			User.findByIdAndUpdate(_id, req.body).exec()
			.then(
			function(user){
				res.json(user);
			},
			function(erro){
				console.error(erro);
				res.status(500).json(erro);
			});
		} else{
			User.create(req.body)
			.then(
			function(user){
				res.status(201).json(user);
			},
			function(erro){
				console.log(erro);
				res.status(500).json(erro)
			});
		}
	}
	return controller;
};