var jwt = require('jwt-simple');
var moment = require('moment');

module.exports = function(app) {

    var User = app.models.user;
    var controller = {};

    controller.gerarToken = function(req, res) {
        if (!req.body.email || !req.body.senha) {
            res.status(401).json("Usuario ou senha inválidos.");
        }

        User.findOne({
                "email": req.body.email
            }).exec()
            .then(
                function(user) {
                    var expires = moment().add(7, 'days').valueOf();
                    var token = jwt.encode({
                        "iss": user.id,
                        "exp": expires
                    }, "saltPassWord");

                    return res.json({
                        "token": token,
                        "expires": expires,
                        "user": user.toJSON()
                    });
                },
                function(erro) {
                    res.status(401).json("Usuario ou senha inválidos.");
                }
            );

    };

    controller.validarToken = function(req, res, next) {
        var model = require('./ModelUsuario'),
            jwt = require('jwt-simple');

        var token = (req.body && req.body.access_token) || (req.query && req.query.access_token) || req.headers['x-access-token'];
        if (token) {
            try {
                var decoded = jwt.decode(token, "saltPassWord");
                console.log('decodando ' + decoded);
                if (decoded.exp <= Date.now()) {
                    res.json(400, {
                        error: 'Acesso Expirado, faça login novamente'
                    });
                }
                //3
                model.findOne({
                    _id: decoded.iss
                }, function(err, user) {
                    if (err)
                        res.status(500).json({
                            message: "erro ao procurar usuario do token."
                        })
                    req.user = user;
                    console.log('achei usuario ' + req.user)
                    return next();
                });
                //4
            }
            catch (err) {
                return res.status(401).json({
                    message: 'Erro: Seu token é inválido'
                });
            }
        }
        else {
            res.json(401, {
                message: 'Token não encontrado ou informado'
            })
        }
    };
    return controller;
}