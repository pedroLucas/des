var mongoose = require('mongoose');

module.exports = function(){
	var schema = mongoose.Schema({
		email: {
			type: String,
			required: true,
			index:{
				unique: true
			}
		},
		senha: {
			type: String,
			required: true
		}
	});	
	
	schema.pre('save', function(next){
		console.log("salvando");
		next();
	});
	
	return mongoose.model('User', schema);
};