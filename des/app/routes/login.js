module.exports = function(app){
    var controller = app.controllers.login;
    
    app.route('/login')
        .post(controller.autenticar);
}