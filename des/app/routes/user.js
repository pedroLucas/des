module.exports = function(app){
	var controller = app.controllers.user;
	app.route('/users')
		.post(controller.save);
}