var express =  require('express');
var load = require('express-load');
var bodyParser = require('body-parser');

module.exports = function(){
	
	var app = express();
	
	//aplicacao
	app.set('ip', process.env.IP);
	app.set('port', process.env.PORT);
	app.use(express.static('./public'));
	
	//-------------------middlewares---------------------
	//template enigne
	app.set('view engine', 'ejs');
	app.set('views', './app/views');
	
	//tratador de requisicoes
	app.use(bodyParser.urlencoded({extended: true}));
	app.use(bodyParser.json());
	app.use(require('method-override')());
	
	//-------------------Arquivos---------------------
	//carrega
	load('models', {cwd: 'app'})
		.then('controllers')
		.then('routes')
		.into(app);
	
	return app;
}