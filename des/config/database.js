var mongoose = require("mongoose");

module.exports = function(uri){
	mongoose.set('debug', true);//mostra o log do mongose
	
	mongoose.connect(uri, {server: {poolSize: 5}});
	
	mongoose.connection.on('connected', function(){
		console.log('Mongose! conectado em ' + uri);
	})
	
	mongoose.connection.on('disconnected', function(){
		console.log('Mongose! disconectado de ' + uri);
	})
	
	mongoose.connection.on('error', function(erro){
		console.log('Mongose! erro de conexão: ' + uri);
	})
	
	process.on('SIGINT', function(){
		mongoose.connection.close(function(){
				console.log('Mngoose! Desconectado pelo termino da aplicação');
				process.exit(0);
			});
	});
};