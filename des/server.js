var http = require('http');
var app = require('./config/express')();
var mongoDb = require('./config/database.js')('mongodb://' + process.env.IP + '/contatooh');

http.createServer(app).listen(app.get('port'), function(){
	console.log('Express Server escutando na porta ' + app.get('port'));
});
